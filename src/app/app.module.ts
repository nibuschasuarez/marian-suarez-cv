import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
//formulario
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { DatosComponent } from './components/datos/datos.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SharedModule } from './components/shared/shared.module';
import { APP_ROUTING } from './app.routes';
// import { AgendaComponent } from './components/agenda-agregar/agenda/agenda.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AgendaComponent } from './components/agenda/agenda.component';
import { AgregarAgendaComponent } from './components/agenda/agregar-agenda/agregar-agenda.component';
import { VerAgendaComponent } from './components/agenda/ver-agenda/ver-agenda.component';
import { InformacionComponent } from './components/informacion/informacion.component';
// import { AgendaAgregarComponent } from './components/agenda-agregar/agenda-agregar.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ContactoComponent,
    DatosComponent,
    NavbarComponent,
    AgendaComponent,
    AgregarAgendaComponent,
    VerAgendaComponent,
    InformacionComponent,


 
  ],
  imports: [
    BrowserModule,
    APP_ROUTING,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
